library(data.table)

fileName <- cat_all[cat_all$c_strat == 'cyl',]$fileName[15]

path <- cat_all[cat_all$fileName==fileName,]$path
Id_value <- cat_all[cat_all$fileName==fileName,]$Id_value


delim <- "\t"

fn <- paste0(path,"\\", fileName)
rd <- suppressMessages(
  suppressWarnings(
    read_delim(fn , delim = delim, skip = 0, n_max = 2, col_names = F)))
rd <- t(rd)
cols2read <- c(1, 2, which(!is.na(str_locate(rd, "Up to Branch Order 0")[,1])))
rd <- fread(file = fn, sep = delim, header = T, skip = 1, select = cols2read)
tree_id <- rd[ID == Id_value,]$ParentID
out <- rd[ParentID == tree_id & !is.na(ID),]

out

