---
title: "PPC - preprocessing"
author:
- affiliation: NuoroForestrySchool
  name: "Matteo Piccolo, Roberto Scotti"
date: "23 ottobre  2018"
output:
  html_document: default
  html_notebook: default
  pdf_document: default
keywords: data wrangling
# subtitle: Just first checks
abstract: TO BE COMPLETED
---

# Introduction
Photograps are saved in differen folders.
To acquire and anaylise EXIF data two tables are generated:
1 PhotoDir, the list of 'paths' to the photos (and the ID)
2 ExifData, the accumulation of 'field', 'value' pairs for all photos

# Analysis of the folders where photos are achived


```r
library(tidyverse)
```

```
## -- Attaching packages ---------------------------------------------------------------- tidyverse 1.2.1 --
```

```
## v ggplot2 3.0.0     v purrr   0.2.5
## v tibble  1.4.2     v dplyr   0.7.6
## v tidyr   0.8.1     v stringr 1.3.1
## v readr   1.1.1     v forcats 0.3.0
```

```
## -- Conflicts ------------------------------------------------------------------- tidyverse_conflicts() --
## x dplyr::filter() masks stats::filter()
## x dplyr::lag()    masks stats::lag()
```

```r
library(fs)
library(fst)

base_dir_main <- "/media/nfs/SpRiViFo/foto_rilievi"
base_dir_tmp <- "/home/piccolomatteo/Scrivania/foto_rilievi"
base_dir <- c(base_dir_main, base_dir_tmp)
#base_dir <- "/media/nfs/SpRiViFo/foto_rilievi/20180702/fc76_77"
# base_dir <- "E:/Foto rilievo"
# base_dir <- "C:/Users/ro/Pictures/Foto/ALBUM"
# base_dir <- "C:/Users/ro/Pictures/Foto/ALBUM/2016WeekVenturina_conOrlando"

psplit <- function(p, bd) matrix(unlist(path_split(path_dir(path_rel(p, bd)))), ncol = 2, byrow = T)

# PhotoDir <- tibble(path0 = dir_ls(base_dir, rec=T, glob=c("*.jpg", "*.JPG")),
PhotoDir <- tibble(path0 = dir_ls(base_dir, rec=T, regexp = "jpg|JPG"),
                   path1 = path_rel(path0, base_dir),
                   folder = dirname(path1),
                   photoFile = basename(path1)
                   ) %>%
  filter(folder != "Rotelle") %>%
  rowid_to_column("ID")
```

```
## Error: [ENOENT] Failed to search directory '/media/nfs/SpRiViFo/foto_rilievi': no such file or directory
```

```r
write_fst(PhotoDir, "PhotoDir.fst")
```

```
## Error in is.data.frame(x): oggetto "PhotoDir" non trovato
```

# Extract EXIF data


```r
library(stringr)

pp <- "/media/nfs/SpRiViFo/foto_rilievi/20180702/fc76_77/IMG_0038.JPG"
# pp <- " C:\\Users\\ro\\Pictures\\Foto\\ALBUM\\2017Natale\\Refrancoreimage1.JPG"

exiftool_cmd <- "exiftool"
# exiftool_cmd <- "C:\\Users\\ro\\Documents\\R\\win-library\\3.5\\exiftoolr\\exiftool\\win_exe\\exiftool"
if(system(paste(exiftool_cmd, "-ver")) == 127L) stop("'exiftool' command not found!")
```

```
## Warning in system(paste(exiftool_cmd, "-ver")): 'exiftool' not found
```

```
## Error in eval(expr, envir, enclos): 'exiftool' command not found!
```

```r
read_exif0 <- function(photo.path, cmd=exiftool_cmd) system(paste(cmd, photo.path), intern = TRUE)

head(read_exif0(pp))
```

```
## Error in system(paste(cmd, photo.path), intern = TRUE): 'exiftool' not found
```

```r
read_exif <- function(photo.path, cmd=exiftool_cmd) {
  x <- photo.path %>%
  # Warning: if path contains spaces 'exiftool' will search for each 'word'!
    paste(cmd, .) %>%
    system(intern = TRUE) %>%
    str_split(":", 2) %>%
    unlist %>%
    matrix(ncol = 2, byrow = T) 
  tibble(field = x[,1], value = x[,2]) %>%
  return
}

# I am afraid it will be long!!
t0 <- Sys.time()
cat(paste("In folder", base_dir, "there are", nrow(PhotoDir),"files.\n"))
```

```
## Error in nrow(PhotoDir): oggetto "PhotoDir" non trovato
```

```r
cat(paste("   Exif data acquisition started at:", t0, "\n"))
```

```
##    Exif data acquisition started at: 2018-10-23 19:24:08
```

```r
ExifData <- PhotoDir %>%
  mutate(exif_df = map(path0, read_exif)) %>%
  select(ID, exif_df) %>%
  unnest
```

```
## Error in eval(lhs, parent, parent): oggetto "PhotoDir" non trovato
```

```r
t1 <- Sys.time()
cat(paste("Exif acquisition ended at", t1,", elapsed time:", (t1 - t0)))
```

```
## Exif acquisition ended at 2018-10-23 19:24:08 , elapsed time: 0.00598621368408203
```

```r
write_fst(ExifData, "ExifData.fst")
```

```
## Error in is.data.frame(x): oggetto "ExifData" non trovato
```
