library(fs)
# PD <- read_fst('PhotoDir.fst')
# ED <- read_fst('ExifData.fst')

last_element <- function(p, pos = 0) {
  vul <- unlist(path_split(p))
  vul[length(vul) - pos]
}
pd1 <- PD %>%
  select(ID, path0) %>%
  mutate(date_dir = map_chr(path0, last_element, pos =2),
         fc_dir = map_chr(path0, last_element, pos =1),
         photoFile = map_chr(path0, last_element, pos =0),
         is_in_NFS = as.integer(!is.na(str_match(path0, "nfs")))
  )

pd1[sample(nrow(pd1), 5),]

distinct(pd1, date_dir)  %>% arrange(date_dir) %>% unlist %>% paste(collapse = " - ")

distinct(pd1, fc_dir) %>% arrange(fc_dir) %>% unlist %>% paste(collapse = " - ")

pd1_wo_dups <- pd1 %>%
  group_by(date_dir, fc_dir, photoFile) %>%
  summarise(frq = n()) %>%
  filter(frq > 1) %>%
  add_column(is_in_NFS = 1) %>%
  anti_join(pd1, .)

PD_old <- PD
PD <- pd1_wo_dups
rm(pd1, pd1_wo_dups)


ED <- PD %>%
  distinct(ID) %>%
  left_join(ED)

