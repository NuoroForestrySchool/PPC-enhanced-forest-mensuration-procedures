# CRP enhanced forest mensuration procedures

Through Close Range Photogrammetry (CRP) forest mensuration procedures, generally demanding in terms of qualified professionals time, can be greatly enhanced, reducing costs while increasing data richness, precision and accuracy.